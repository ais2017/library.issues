package mephi.dozen.ais;

import mephi.dozen.ais.entity.user.User;

import java.util.ArrayList;

public class AuthContext {

    public static ArrayList<User> authorizedUsers = new ArrayList<>();

    public static User login (User user) {
        authorizedUsers.add(user);
        return user;
    }

    public static boolean checkIfUserAuthorized(String login) {
        for (User user : authorizedUsers) {
            if (user.getLogin().equals(login)) {
                return true;
            }
        }
        return false;
    }

    public static User logout(String login) {
        for (User user : authorizedUsers) {
            if (user.getLogin().equals(login)) {
                authorizedUsers.remove(user);
                return user;
            }
        }
        return null;
    }

    public static User getCurrentUser() {
        if (authorizedUsers.isEmpty()) {
            return null;
        } else {
            return authorizedUsers.get(0);
        }
    }

}
