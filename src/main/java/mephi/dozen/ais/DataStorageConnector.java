package mephi.dozen.ais;

import mephi.dozen.ais.entity.ClientOrder;
import mephi.dozen.ais.entity.PublishingOffice;
import mephi.dozen.ais.entity.Request;
import mephi.dozen.ais.entity.book.BookForLibrary;
import mephi.dozen.ais.entity.book.BookForRequest;
import mephi.dozen.ais.entity.user.User;

import java.util.ArrayList;

public class DataStorageConnector {

    private static ArrayList<User> users = new ArrayList<>();
    private static ArrayList<ClientOrder> clientOrders = new ArrayList<>();
    private static ArrayList<Request> requests = new ArrayList<>();
    private static ArrayList<PublishingOffice> publishingOffices = new ArrayList<>();
    private static ArrayList<BookForLibrary> booksForLibrary = new ArrayList<>();

    public static User saveUser (User user) {
        users.add(user);
        return user;
    }

    public static User updateUser(User user) {
        deleteUser(user.getLogin());
        return saveUser(user);
    }

    public static User deleteUser(String login) {
        for (User user : users) {
            if (user.getLogin().equals(login)) {
                users.remove(user);
                return user;
            }
        }
        return null;
    }

    public static User getUser(String login) {
        for (User user : users) {
            if (user.getLogin().equals(login)) {
                return user;
            }
        }
        return null;
    }

    public static PublishingOffice savePublishingOffice(PublishingOffice publishingOffice) {
        publishingOffices.add(publishingOffice);
        return publishingOffice;
    }

    public static PublishingOffice updatePublishingOffice(PublishingOffice publishingOffice) {
        deletePublishingOffice(publishingOffice.getName());
        return savePublishingOffice(publishingOffice);
    }

    public static PublishingOffice deletePublishingOffice(String name) {
        for (PublishingOffice publishingOffice : publishingOffices) {
            if (publishingOffice.getName().equals(name)) {
                publishingOffices.remove(publishingOffice);
                return publishingOffice;
            }
        }
        return null;
    }

    public static PublishingOffice getPublishingOffice(String name) {
        for (PublishingOffice publishingOffice : publishingOffices) {
            if (publishingOffice.getName().equals(name)) {
                return publishingOffice;
            }
        }
        return null;
    }

    public static Request saveRequest(Request request) {
        requests.add(request);
        return request;
    }

    public static ArrayList<Request> getRequests() {
        return requests;
    }

    public static Request updateRequest(Request request) {
        deleteRequest(request.getId());
        return saveRequest(request);
    }

    public static Request deleteRequest(Long id) {
        for (Request request : requests) {
            if (request.getId().equals(id)) {
                requests.remove(request);
                return request;
            }
        }
        return null;
    }

    public static Request getRequest(Long id) {
        for (Request request : requests) {
            if (request.getId().equals(id)) {
                return request;
            }
        }
        return null;
    }

    public static BookForLibrary handleBookForRequest(BookForRequest book) {
        BookForLibrary entity = getBookForLibrary(book.getTitle(), book.getAuthor());
        if (entity == null) {
            BookForLibrary bookForLibrary = new BookForLibrary(book.getPublishingOffice(), book.getTitle(), book.getAuthor(),
                    book.getAmount(), book.getHyperlink());
            booksForLibrary.add(bookForLibrary);
            return bookForLibrary;
        }
        booksForLibrary.remove(entity);
        if (!book.getEBook()) {
            if (entity.getAmountInLibrary() == null) {
                entity.setAmountInLibrary(book.getAmount());
            } else {
                entity.changeAmountInLibrary(book.getAmount());
            }
        }
        if (book.getHyperlink() != null && entity.getHyperlink() == null) {
            entity.setHyperlink(book.getHyperlink());
        }
        booksForLibrary.add(entity);
        return entity;
    }

    public static BookForLibrary getBookForLibrary(String title, String author) {
        for (BookForLibrary book : booksForLibrary) {
            if (book.getTitle().equals(title) && book.getAuthor().equals(author)) {
                return book;
            }
        }
        return null;
    }

    public static ArrayList<BookForLibrary> getBooks() {
        return booksForLibrary;
    }

    public static ClientOrder saveClientOrder(ClientOrder clientOrder) {
        clientOrders.add(clientOrder);
        return clientOrder;
    }

    public static ClientOrder deleteClientOrder(Long id) {
        ClientOrder clientOrder = getClientOrder(id);
        if (clientOrder != null) {
            clientOrders.remove(clientOrder);
            return clientOrder;
        }
        return null;
    }

    public static ClientOrder getClientOrder(Long id) {
        for (ClientOrder clientOrder: clientOrders) {
            if (clientOrder.getId().equals(id)) {
                return clientOrder;
            }
        }
        return null;
    }
}
