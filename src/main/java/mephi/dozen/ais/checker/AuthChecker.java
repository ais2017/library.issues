package mephi.dozen.ais.checker;

import mephi.dozen.ais.AuthContext;
import mephi.dozen.ais.DataStorageConnector;
import mephi.dozen.ais.entity.user.Administrator;
import mephi.dozen.ais.info.RegisterRequestInfo;
import mephi.dozen.ais.utils.RegexUtils;

import java.util.ArrayList;

public class AuthChecker {

    private static String LOGIN_PATTERN = "^[a-zA-Z]\\w{4,49}$";
    private static String NAME_PATTERN = "^.{4,49}$";
    private static String PASSWORD_PATTERN = "^.{5,50}$";

    public static ArrayList<String> checkRegisterRequestInfo(RegisterRequestInfo info) {
        ArrayList<String> errorMessages = new ArrayList<>();

        if (AuthContext.getCurrentUser() == null || !(AuthContext.getCurrentUser() instanceof Administrator)) {
            errorMessages.add("Не достаточно прав для выполнения операции");
            return errorMessages;
        }

        if (info.getLogin() == null || info.getLogin().trim().isEmpty()) {
            errorMessages.add("Не заполнен логин пользователя");
        }
        if (info.getName() == null || info.getName().trim().isEmpty()) {
            errorMessages.add("Не заполнено имя пользователя");
        }
        if (info.getPassword() == null || info.getPassword().trim().isEmpty()) {
            errorMessages.add("Не заполнен пароль");
        }
        if (info.getUserType() == null) {
            errorMessages.add("Не заполнен тип пользователя");
        }
        if (!errorMessages.isEmpty()) {
            return errorMessages;
        }

        if (!RegexUtils.checkWithRegExp(info.getLogin(), LOGIN_PATTERN)) {
            errorMessages.add("Логин пользователя не соответствует регулярному выражению " + LOGIN_PATTERN);
        }
        if (!RegexUtils.checkWithRegExp(info.getName(), NAME_PATTERN)) {
            errorMessages.add("Имя пользователя не соответствует регулярному выражению " + NAME_PATTERN);
        }
        if (!RegexUtils.checkWithRegExp(info.getPassword(), PASSWORD_PATTERN)) {
            errorMessages.add("Пароль не соответствует регулярному выражению " + PASSWORD_PATTERN);
        }
        if (!errorMessages.isEmpty()) {
            return errorMessages;
        }

        if (DataStorageConnector.getUser(info.getLogin()) != null) {
            errorMessages.add("Пользователь с таким логином уже существует");
        }
        return errorMessages;
    }

    public static ArrayList<String> checkRegisterRequestInfoBeforeUpdate(RegisterRequestInfo info) {
        ArrayList<String> errorMessages = new ArrayList<>();

        if (AuthContext.getCurrentUser() == null || !(AuthContext.getCurrentUser() instanceof Administrator)) {
            errorMessages.add("Не достаточно прав для выполнения операции");
            return errorMessages;
        }

        if (info.getLogin() == null) {
            errorMessages.add("Не указан логин пользователя");
        }
        if (info.getUserType() != null) {
            errorMessages.add("Невозможно изменить тип пользователя. Поле должно быть пустым.");
        }
        if (info.getName() == null && info.getPassword() == null) {
            errorMessages.add("Нет данных для обновления");
        }
        if (!errorMessages.isEmpty()) {
            return errorMessages;
        }

        if (DataStorageConnector.getUser(info.getLogin()) == null) {
            errorMessages.add("Пользователя с таким логином не существует");
            return errorMessages;
        }

        if (info.getName() != null && !RegexUtils.checkWithRegExp(info.getName(), NAME_PATTERN)) {
            errorMessages.add("Имя пользователя не соответствует регулярному выражению " + NAME_PATTERN);
        }
        if (info.getPassword() != null && !RegexUtils.checkWithRegExp(info.getPassword(), PASSWORD_PATTERN)) {
            errorMessages.add("Пароль не соответствует регулярному выражению " + PASSWORD_PATTERN);
        }
        if (!errorMessages.isEmpty()) {
            return errorMessages;
        }

        return errorMessages;
    }

    public static ArrayList<String> checkLoginBeforeDelete(String login) {
        ArrayList<String> errorMessages = new ArrayList<>();

        if (AuthContext.getCurrentUser() == null || !(AuthContext.getCurrentUser() instanceof Administrator)) {
            errorMessages.add("Не достаточно прав для выполнения операции");
            return errorMessages;
        }

        if (login == null || login.trim().isEmpty()) {
            errorMessages.add("Не указан логин пользователя");
        }

        return errorMessages;
    }

    public static ArrayList<String> checkLoginInfo(String login, String password) {
        ArrayList<String> errorMessages = new ArrayList<>();

        if (login == null || login.equals("")) {
            errorMessages.add("Не заполнен логин пользователя");
        }
        if (login == null || login.equals("")) {
            errorMessages.add("Не заполнен пароль");
        }
        return errorMessages;
    }

    public static ArrayList<String> checkLogoutInfo(String login) {
        ArrayList<String> errorMessages = new ArrayList<>();

        if (login == null || login.equals("")) {
            errorMessages.add("Не заполнен логин пользователя");
        }
        return errorMessages;
    }

}
