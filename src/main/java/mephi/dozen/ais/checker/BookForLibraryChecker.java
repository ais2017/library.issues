package mephi.dozen.ais.checker;

import mephi.dozen.ais.AuthContext;
import mephi.dozen.ais.DataStorageConnector;
import mephi.dozen.ais.entity.PublishingOffice;
import mephi.dozen.ais.entity.book.BookForLibrary;
import mephi.dozen.ais.entity.user.Administrator;
import mephi.dozen.ais.entity.user.BookManager;
import mephi.dozen.ais.utils.RegexUtils;

import java.util.ArrayList;

/**
 * @author Mitenkov Roman
 */
public class BookForLibraryChecker {

    public static ArrayList<String> checkBook(BookForLibrary book) {
        ArrayList<String> errorMessages = new ArrayList<>();

        if (book.getTitle() == null) {
            errorMessages.add("Не указан заголовок книги");
        }
        if (book.getAmountInLibrary() == null && book.getHyperlink() == null) {
            errorMessages.add("Не указано количество книг или ссылка");
        }
        if (book.getAuthor() == null) {
            errorMessages.add("Не указан автор книги");
        }
        if (!errorMessages.isEmpty()) {
            return errorMessages;
        }

        if (book.getAmountInLibrary() != null && book.getAmountInLibrary() <= 0) {
            errorMessages.add("Количество книг должно быть положительным");
        }
        return errorMessages;
    }

}
