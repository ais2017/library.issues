package mephi.dozen.ais.checker;

import mephi.dozen.ais.entity.book.BookForRequest;

import java.util.ArrayList;

public class BookForRequestChecker {

    public static ArrayList<String> checkBookForRequestBeforeSend(BookForRequest book) {

        ArrayList<String> errorMessages = new ArrayList<>();
        if (book.getTitle() == null) {
            errorMessages.add("Не указан заголовок книги");
        }
        if (book.getEBook() == null) {
            errorMessages.add("Не указан вид книги");
        }
        if (book.getAuthor() == null) {
            errorMessages.add("Не указан автор книги");
        }
        if (!errorMessages.isEmpty()) {
            return errorMessages;
        }

        if (!book.getEBook() && book.getAmount() == null) {
            errorMessages.add("Не указано количество книг");
        }
        if (!book.getEBook() && book.getAmount() != null && book.getAmount() <= 0) {
            errorMessages.add("Количество книг должно быть положительным");
        }
        return errorMessages;
    }

    public static ArrayList<String> checkBookForRequestAfterSend(BookForRequest book) {

        ArrayList<String> errorMessages = new ArrayList<>();
        if (book.getTitle() == null) {
            errorMessages.add("Не указан заголовок книги");
        }
        if (book.getEBook() == null) {
            errorMessages.add("Не указан вид книги");
        }
        if (book.getAuthor() == null) {
            errorMessages.add("Не указан автор книги");
        }
        if (!errorMessages.isEmpty()) {
            return errorMessages;
        }

        if (!book.getEBook() && book.getAmount() == null) {
            errorMessages.add("Не указано количество книг");
        }
        if (!book.getEBook() && book.getAmount() != null && book.getAmount() <= 0) {
            errorMessages.add("Количество книг должно быть положительным");
        }
        if (book.getEBook() && book.getHyperlink() == null) {
            errorMessages.add("Не указана ссылка на электронную книгу");
        }
        return errorMessages;
    }
}
