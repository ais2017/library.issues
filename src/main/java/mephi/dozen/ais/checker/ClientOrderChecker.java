package mephi.dozen.ais.checker;

import mephi.dozen.ais.AuthContext;
import mephi.dozen.ais.DataStorageConnector;
import mephi.dozen.ais.entity.ClientOrder;
import mephi.dozen.ais.entity.book.BookForRequest;
import mephi.dozen.ais.entity.user.Administrator;

import java.util.ArrayList;

public class ClientOrderChecker {

    public static ArrayList<String> checkClientOrderBeforeCreate(ClientOrder info) {

        ArrayList<String> errorMessages = new ArrayList<>();
        if (AuthContext.getCurrentUser() == null || (AuthContext.getCurrentUser() instanceof Administrator)) {
            errorMessages.add("Не достаточно прав для выполнения операции");
            return errorMessages;
        }

        if (info.getAction() == null) {
            errorMessages.add("Не указано действие");
        }
        if (info.getBooks() == null || info.getBooks().isEmpty()) {
            errorMessages.add("Не указано ни одной книги");
            return errorMessages;
        }

        int i = 1;
        for (BookForRequest book : info.getBooks()) {
            if (!BookForRequestChecker.checkBookForRequestBeforeSend(book).isEmpty()) {
                errorMessages.add("Формат книги №" + i + " не верный");
            }
            i += 1;
        }
        return errorMessages;
    }

    public static ArrayList<String> checkClientOrderBeforeDelete(Long id) {

        ArrayList<String> errorMessages = new ArrayList<>();
        if (AuthContext.getCurrentUser() == null || (AuthContext.getCurrentUser() instanceof Administrator)) {
            errorMessages.add("Не достаточно прав для выполнения операции");
            return errorMessages;
        }

        if (id == null) {
            errorMessages.add("Не указан идентификатор");
            return errorMessages;
        }
        if (DataStorageConnector.getClientOrder(id) == null) {
            errorMessages.add("Заявки с указанным идентификатором не найдено");
        }

        return errorMessages;
    }

}
