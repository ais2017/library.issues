package mephi.dozen.ais.checker;

import mephi.dozen.ais.AuthContext;
import mephi.dozen.ais.DataStorageConnector;
import mephi.dozen.ais.entity.PublishingOffice;
import mephi.dozen.ais.entity.book.BookForLibrary;
import mephi.dozen.ais.entity.user.Administrator;
import mephi.dozen.ais.entity.user.BookManager;
import mephi.dozen.ais.utils.RegexUtils;

import java.util.ArrayList;

/**
 * @author Mitenkov Roman
 */
public class PublishingOfficeChecker {

    private static String NAME_PATTERN = "^.{5,100}$";
    private static String PHONE_PATTERN = "^\\d{5,11}$";

    public static ArrayList<String> checkPublishingOfficeBeforeCreate(PublishingOffice publishingOffice) {
        ArrayList<String> errorMessages = new ArrayList<>();

        if (AuthContext.getCurrentUser() == null ||
                (!(AuthContext.getCurrentUser() instanceof Administrator) && !(AuthContext.getCurrentUser() instanceof BookManager))) {
            errorMessages.add("Не достаточно прав для выполнения операции");
            return errorMessages;
        }

        if (publishingOffice.getName() == null || publishingOffice.getName().trim().isEmpty()) {
            errorMessages.add("Не заполнено название издательства");
        }
        if (publishingOffice.getPhone() == null || publishingOffice.getPhone().trim().isEmpty()) {
            errorMessages.add("Не заполнен телефон издательства");
        }
        if (publishingOffice.getBooks() != null && !publishingOffice.getBooks().isEmpty()) {
            errorMessages.add("Список книг не должен быть заполнен");
        }
        if (!errorMessages.isEmpty()) {
            return errorMessages;
        }

        if (!RegexUtils.checkWithRegExp(publishingOffice.getName(), NAME_PATTERN)) {
            errorMessages.add("Название организации не соответствует регулярному выражению " + NAME_PATTERN);
        }
        if (!RegexUtils.checkWithRegExp(publishingOffice.getPhone(), PHONE_PATTERN)) {
            errorMessages.add("Телефон не соответствует регулярному выражению " + PHONE_PATTERN);
        }
        if (!errorMessages.isEmpty()) {
            return errorMessages;
        }

        if (DataStorageConnector.getPublishingOffice(publishingOffice.getName()) != null) {
            errorMessages.add("Издательство с таким названием уже существует");
        }
        return errorMessages;
    }

    public static ArrayList<String> checkPublishingOfficeBeforeUpdate(PublishingOffice publishingOffice) {
        ArrayList<String> errorMessages = new ArrayList<>();

        if (AuthContext.getCurrentUser() == null ||
                (!(AuthContext.getCurrentUser() instanceof Administrator) && !(AuthContext.getCurrentUser() instanceof BookManager))) {
            errorMessages.add("Не достаточно прав для выполнения операции");
            return errorMessages;
        }

        if (publishingOffice.getName() == null || publishingOffice.getName().trim().isEmpty()) {
            errorMessages.add("Не заполнено название издательства");
        }
        else if (DataStorageConnector.getPublishingOffice(publishingOffice.getName()) == null) {
            errorMessages.add("Издательства с таким названием не существует");
        }
        if (publishingOffice.getPhone() == null) {
            errorMessages.add("Отсутствуют данные для обновления");
        }
        if (publishingOffice.getBooks() != null) {
            errorMessages.add("Список книг не должен быть заполнен");
        }
        if (!errorMessages.isEmpty()) {
            return errorMessages;
        }

        if (publishingOffice.getPhone() != null && !RegexUtils.checkWithRegExp(publishingOffice.getPhone(), PHONE_PATTERN)) {
            errorMessages.add("Телефон не соответствует регулярному выражению " + PHONE_PATTERN);
        }

        return errorMessages;
    }

    public static ArrayList<String> checkPublishingOfficeBeforeDelete(String name) {
        ArrayList<String> errorMessages = new ArrayList<>();

        if (AuthContext.getCurrentUser() == null ||
                (!(AuthContext.getCurrentUser() instanceof Administrator) && !(AuthContext.getCurrentUser() instanceof BookManager))) {
            errorMessages.add("Не достаточно прав для выполнения операции");
            return errorMessages;
        }

        if (name == null || name.trim().isEmpty()) {
            errorMessages.add("Не заполнено название издательства");
        }
        else if (DataStorageConnector.getPublishingOffice(name) == null) {
            errorMessages.add("Издательства с таким названием не существует");
        }

        return errorMessages;
    }

    public static ArrayList<String> checkPublishingOfficeBeforeAddBooks(PublishingOffice publishingOffice) {
        ArrayList<String> errorMessages = new ArrayList<>();

        if (AuthContext.getCurrentUser() == null ||
                (!(AuthContext.getCurrentUser() instanceof Administrator) && !(AuthContext.getCurrentUser() instanceof BookManager))) {
            errorMessages.add("Не достаточно прав для выполнения операции");
            return errorMessages;
        }

        if (publishingOffice.getName() == null || publishingOffice.getName().trim().isEmpty()) {
            errorMessages.add("Не заполнено название издательства");
        }
        else if (DataStorageConnector.getPublishingOffice(publishingOffice.getName()) == null) {
            errorMessages.add("Издательства с таким названием не существует");
        }

        int i = 1;
        for (BookForLibrary book : publishingOffice.getBooks()) {
            if (!BookForLibraryChecker.checkBook(book).isEmpty()) {
                errorMessages.add("Формат книги №" + i + " не верный");
            }
            i += 1;
        }

        return errorMessages;
    }

    public static ArrayList<String> checkPublishingOfficeBeforeRemoveBooks(PublishingOffice publishingOffice) {
        ArrayList<String> errorMessages = new ArrayList<>();

        if (AuthContext.getCurrentUser() == null ||
                (!(AuthContext.getCurrentUser() instanceof Administrator) && !(AuthContext.getCurrentUser() instanceof BookManager))) {
            errorMessages.add("Не достаточно прав для выполнения операции");
            return errorMessages;
        }

        if (publishingOffice.getName() == null || publishingOffice.getName().trim().isEmpty()) {
            errorMessages.add("Не заполнено название издательства");
        }
        else if (DataStorageConnector.getPublishingOffice(publishingOffice.getName()) == null) {
            errorMessages.add("Издательства с таким названием не существует");
        }

        return errorMessages;
    }
}
