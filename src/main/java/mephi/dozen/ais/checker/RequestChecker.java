package mephi.dozen.ais.checker;

import mephi.dozen.ais.AuthContext;
import mephi.dozen.ais.DataStorageConnector;
import mephi.dozen.ais.entity.Request;
import mephi.dozen.ais.entity.book.BookForRequest;
import mephi.dozen.ais.entity.user.Administrator;
import mephi.dozen.ais.entity.user.BookManager;
import mephi.dozen.ais.utils.RegexUtils;

import java.util.ArrayList;

/**
 * @author Mitenkov Roman
 */
public class RequestChecker {

    public static ArrayList<String> checkRequestBeforeSend(Request request) {
        ArrayList<String> errorMessages = new ArrayList<>();

        if (AuthContext.getCurrentUser() == null || !(AuthContext.getCurrentUser() instanceof BookManager)) {
            errorMessages.add("Не достаточно прав для выполнения операции");
            return errorMessages;
        }

        if (request.getPublishingOffice() == null) {
            errorMessages.add("Не указано издательство");
        }
        if (request.getBooks() == null || request.getBooks().isEmpty()) {
            errorMessages.add("Не указаны книги");
        }
        if (!errorMessages.isEmpty()) {
            return errorMessages;
        }

        int i = 1;
        for (BookForRequest book : request.getBooks()) {
            if (!BookForRequestChecker.checkBookForRequestBeforeSend(book).isEmpty()) {
                errorMessages.add("Формат книги №" + i + " не верный");
            }
            i += 1;
        }
        return errorMessages;
    }

    public static ArrayList<String> checkRequestAfterSend(Request request) {
        ArrayList<String> errorMessages = new ArrayList<>();

        if (AuthContext.getCurrentUser() == null || !(AuthContext.getCurrentUser() instanceof BookManager)) {
            errorMessages.add("Не достаточно прав для выполнения операции");
            return errorMessages;
        }

        if (request.getPublishingOffice() == null) {
            errorMessages.add("Не указано издательство");
        }
        if (request.getBooks() == null || request.getBooks().isEmpty()) {
            errorMessages.add("Не указаны книги");
        }
        if (!errorMessages.isEmpty()) {
            return errorMessages;
        }

        int i = 1;
        for (BookForRequest book : request.getBooks()) {
            if (!BookForRequestChecker.checkBookForRequestAfterSend(book).isEmpty()) {
                errorMessages.add("Формат книги №" + i + " не верный");
            }
            i += 1;
        }
        return errorMessages;
    }
}
