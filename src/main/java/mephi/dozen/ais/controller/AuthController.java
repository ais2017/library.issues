package mephi.dozen.ais.controller;

import mephi.dozen.ais.checker.AuthChecker;
import mephi.dozen.ais.response.Response;
import mephi.dozen.ais.info.RegisterRequestInfo;
import mephi.dozen.ais.service.AuthService;
import mephi.dozen.ais.entity.user.User;

import java.util.ArrayList;

public class AuthController {

    public static Response<User> register(RegisterRequestInfo info) {
        Response<User> response = new Response<>();
        ArrayList<String> errorMessages = AuthChecker.checkRegisterRequestInfo(info);
        if (!errorMessages.isEmpty()) {
            response.setErrorMessages(errorMessages);
            return response;
        }

        User user = AuthService.register(info);
        if (user != null) {
            response.setField(user);
        }
        else {
            response.addErrorMessage("Ошибка при создании пользователя");
        }

        return response;
    }

    public static Response<User> login(String login, String password) {
        Response<User> response = new Response<>();
        ArrayList<String> errorMessages = AuthChecker.checkLoginInfo(login, password);
        if (!errorMessages.isEmpty()) {
            response.setErrorMessages(errorMessages);
            return response;
        }

        User user = AuthService.login(login, password);
        if (user != null) {
            response.setField(user);
        }
        else {
            response.addErrorMessage("Ошибка при входе в систему");
        }

        return response;
    }

    public static Response<User> logout(String login) {
        Response<User> response = new Response<>();
        ArrayList<String> errorMessages = AuthChecker.checkLogoutInfo(login);
        if (!errorMessages.isEmpty()) {
            response.setErrorMessages(errorMessages);
            return response;
        }

        User user = AuthService.logout(login);
        if (user != null) {
            response.setField(user);
        }
        else {
            response.addErrorMessage("Ошибка при выходе из системы");
        }

        return response;
    }

    public static Response<User> update(RegisterRequestInfo info) {
        Response<User> response = new Response<>();
        ArrayList<String> errorMessages = AuthChecker.checkRegisterRequestInfoBeforeUpdate(info);
        if (!errorMessages.isEmpty()) {
            response.setErrorMessages(errorMessages);
            return response;
        }

        User user = AuthService.update(info);
        if (user != null) {
            response.setField(user);
        }
        else {
            response.addErrorMessage("Ошибка при обновлении пользователя");
        }

        return response;
    }

    public static Response<User> delete(String login) {
        Response<User> response = new Response<>();
        ArrayList<String> errorMessages = AuthChecker.checkLoginBeforeDelete(login);
        if (!errorMessages.isEmpty()) {
            response.setErrorMessages(errorMessages);
            return response;
        }

        User user = AuthService.delete(login);
        if (user != null) {
            response.setField(user);
        }
        else {
            response.addErrorMessage("Ошибка при удалении пользователя");
        }

        return response;
    }
}
