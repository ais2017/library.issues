package mephi.dozen.ais.controller;

import mephi.dozen.ais.checker.ClientOrderChecker;
import mephi.dozen.ais.entity.ClientOrder;
import mephi.dozen.ais.response.Response;
import mephi.dozen.ais.service.ClientOrderService;

import java.util.ArrayList;

public class ClientOrderController {

    public static Response<ClientOrder> create(ClientOrder info) {
        Response<ClientOrder> response = new Response<>();
        ArrayList<String> errorMessages = ClientOrderChecker.checkClientOrderBeforeCreate(info);
        if (!errorMessages.isEmpty()) {
            response.setErrorMessages(errorMessages);
            return response;
        }

        ClientOrder clientOrder = ClientOrderService.create(info);
        if (clientOrder != null) {
            response.setField(clientOrder);
        }
        else {
            response.addErrorMessage("Ошибка при создании заявки");
        }
        return response;
    }

    public static Response<ClientOrder> delete(Long id) {
        Response<ClientOrder> response = new Response<>();
        ArrayList<String> errorMessages = ClientOrderChecker.checkClientOrderBeforeDelete(id);
        if (!errorMessages.isEmpty()) {
            response.setErrorMessages(errorMessages);
            return response;
        }

        ClientOrder clientOrder = ClientOrderService.delete(id);
        if (clientOrder != null) {
            response.setField(clientOrder);
        }
        else {
            response.addErrorMessage("Ошибка при удалении заявки");
        }
        return response;
    }
}
