package mephi.dozen.ais.controller;

import mephi.dozen.ais.checker.AuthChecker;
import mephi.dozen.ais.checker.PublishingOfficeChecker;
import mephi.dozen.ais.entity.PublishingOffice;
import mephi.dozen.ais.response.Response;
import mephi.dozen.ais.service.PublishingOfficeService;

import java.util.ArrayList;

/**
 * @author Mitenkov Roman
 */
public class PublishingOfficeController {

    public static Response<PublishingOffice> addPublishingOffice(PublishingOffice publishingOffice) {
        Response<PublishingOffice> response = new Response<>();
        ArrayList<String> errorMessages = PublishingOfficeChecker.checkPublishingOfficeBeforeCreate(publishingOffice);
        if (!errorMessages.isEmpty()) {
            response.setErrorMessages(errorMessages);
            return response;
        }

        publishingOffice = PublishingOfficeService.add(publishingOffice);
        if (publishingOffice != null) {
            response.setField(publishingOffice);
        }
        else {
            response.addErrorMessage("Ошибка при добавлении издательства");
        }

        return response;
    }

    public static Response<PublishingOffice> update(PublishingOffice publishingOffice) {
        Response<PublishingOffice> response = new Response<>();
        ArrayList<String> errorMessages = PublishingOfficeChecker.checkPublishingOfficeBeforeUpdate(publishingOffice);
        if (!errorMessages.isEmpty()) {
            response.setErrorMessages(errorMessages);
            return response;
        }

        publishingOffice = PublishingOfficeService.update(publishingOffice);
        if (publishingOffice != null) {
            response.setField(publishingOffice);
        }
        else {
            response.addErrorMessage("Ошибка при обновлении информации об издательстве");
        }

        return response;
    }

    public static Response<PublishingOffice> deletePublishingOffice(String name) {
        Response<PublishingOffice> response = new Response<>();
        ArrayList<String> errorMessages = PublishingOfficeChecker.checkPublishingOfficeBeforeDelete(name);
        if (!errorMessages.isEmpty()) {
            response.setErrorMessages(errorMessages);
            return response;
        }

        PublishingOffice publishingOffice = PublishingOfficeService.delete(name);
        if (publishingOffice != null) {
            response.setField(publishingOffice);
        }
        else {
            response.addErrorMessage("Ошибка при удалении издательства");
        }

        return response;
    }

    public static Response<PublishingOffice> addBooksToPublishingOffice(PublishingOffice publishingOffice) {
        Response<PublishingOffice> response = new Response<>();
        ArrayList<String> errorMessages = PublishingOfficeChecker.checkPublishingOfficeBeforeAddBooks(publishingOffice);
        if (!errorMessages.isEmpty()) {
            response.setErrorMessages(errorMessages);
            return response;
        }

        publishingOffice = PublishingOfficeService.addBooks(publishingOffice);
        if (publishingOffice != null) {
            response.setField(publishingOffice);
        }
        else {
            response.addErrorMessage("Ошибка при добавлении книг в издательство");
        }

        return response;
    }

    public static Response<PublishingOffice> removeBooksToPublishingOffice(PublishingOffice publishingOffice) {
        Response<PublishingOffice> response = new Response<>();
        ArrayList<String> errorMessages = PublishingOfficeChecker.checkPublishingOfficeBeforeRemoveBooks(publishingOffice);
        if (!errorMessages.isEmpty()) {
            response.setErrorMessages(errorMessages);
            return response;
        }

        publishingOffice = PublishingOfficeService.removeBooks(publishingOffice);
        if (publishingOffice != null) {
            response.setField(publishingOffice);
        }
        else {
            response.addErrorMessage("Ошибка при удалении книг из издательство");
        }

        return response;
    }
}
