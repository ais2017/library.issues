package mephi.dozen.ais.controller;

import mephi.dozen.ais.checker.PublishingOfficeChecker;
import mephi.dozen.ais.checker.RequestChecker;
import mephi.dozen.ais.entity.Request;
import mephi.dozen.ais.response.Response;
import mephi.dozen.ais.service.RequestService;

import java.util.ArrayList;

/**
 * @author Mitenkov Roman
 */
public class RequestController {

    public static Response<Request> sendRequest(Request request) {
        Response<Request> response = new Response<>();
        ArrayList<String> errorMessages = RequestChecker.checkRequestBeforeSend(request);
        if (!errorMessages.isEmpty()) {
            response.setErrorMessages(errorMessages);
            return response;
        }

        request = RequestService.send(request);
        if (request != null) {
            response.setField(request);
        }
        else {
            response.addErrorMessage("Ошибка при отправке запроса в издательство");
        }

        return response;
    }

    public static Response<Request> handleResponse(Request request) {
        Response<Request> response = new Response<>();
        ArrayList<String> errorMessages = RequestChecker.checkRequestAfterSend(request);
        if (!errorMessages.isEmpty()) {
            response.setErrorMessages(errorMessages);
            return response;
        }

        request = RequestService.handleResponse(request);
        if (request != null) {
            response.setField(request);
        }
        else {
            response.addErrorMessage("Ошибка при обработке ответа из издательства");
        }

        return response;
    }
}
