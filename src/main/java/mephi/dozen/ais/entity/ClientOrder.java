package mephi.dozen.ais.entity;

import mephi.dozen.ais.AuthContext;
import mephi.dozen.ais.entity.book.BookForRequest;
import mephi.dozen.ais.entity.user.User;

import java.util.ArrayList;

public class ClientOrder {

    public static final String CREATED = "created";
    public static final String FINISHED = "finished";

    public static final String addBooks = "addBooks";
    public static final String removeBooks = "removeBooks";

    private Long id;

    private User createdBy;

    private String state;

    private String action;

    private ArrayList<BookForRequest> books;

    private static Long counter = 1L;

    public ClientOrder(String action, ArrayList<BookForRequest> books) {
        this.createdBy = AuthContext.getCurrentUser();
        this.action = action;
        this.books = books;
        this.id = counter;
        counter += 1;
    }

    public void addBook(BookForRequest book) {
        books.add(book);
    }

    public void removeBook(BookForRequest book){
        books.remove(book);
    }

    public boolean checkIfBookAdded(BookForRequest book) {
        return books.contains(book);
    }

    public Long getId() {
        return id;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public ArrayList<BookForRequest> getBooks() {
        return books;
    }
}
