package mephi.dozen.ais.entity;

import mephi.dozen.ais.entity.book.BookForLibrary;

import java.util.ArrayList;

public class PublishingOffice {

    private Long id;

    private String name;

    private String phone;

    private ArrayList<BookForLibrary> books;

    public PublishingOffice(String name, String phone) {
        this.name = name;
        this.phone = phone;
        books = new ArrayList<>();
    }

    public void addBook(BookForLibrary book) {
        books.add(book);
    }

    public void removeBook(BookForLibrary book){
        books.remove(book);
    }

    public boolean checkIfBookAdded(BookForLibrary book) {
        return books.contains(book);
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public ArrayList<BookForLibrary> getBooks() {
        return books;
    }
}
