package mephi.dozen.ais.entity;

import mephi.dozen.ais.AuthContext;
import mephi.dozen.ais.entity.book.BookForRequest;
import mephi.dozen.ais.entity.user.BookManager;

import java.util.ArrayList;

public class Request {

    public static final String REQUEST_SENDED = "request sended";
    public static final String REQUEST_FINISHED = "request finished";
    public static final String REQUEST_CREATED = "request created";

    private Long id;

    private BookManager createdBy;

    private PublishingOffice publishingOffice;

    private String state;

    private ArrayList<BookForRequest> books;

    public static Long counter = 1L;

    public Request(PublishingOffice publishingOffice) {
        this.id = counter;
        counter += 1;
        this.createdBy = (BookManager) AuthContext.getCurrentUser();
        this.publishingOffice = publishingOffice;
        this.books = new ArrayList<>();
    }

    public void addBook(BookForRequest book) {
        books.add(book);
    }

    public void removeBook(BookForRequest book){
        books.remove(book);
    }

    public void setBooks(ArrayList<BookForRequest> books) {
        this.books = books;
    }

    public boolean checkIfBookAdded(BookForRequest book) {
        return books.contains(book);
    }

    public Long getId() {
        return id;
    }

    public BookManager getCreatedBy() {
        return createdBy;
    }

    public PublishingOffice getPublishingOffice() {
        return publishingOffice;
    }

    public void setPublishingOffice(PublishingOffice publishingOffice) {
        this.publishingOffice = publishingOffice;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public ArrayList<BookForRequest> getBooks() {
        return books;
    }
}
