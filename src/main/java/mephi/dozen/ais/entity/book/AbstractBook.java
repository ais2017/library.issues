package mephi.dozen.ais.entity.book;

import mephi.dozen.ais.entity.PublishingOffice;

import java.util.Date;

public abstract class AbstractBook {

    private Long id;

    private PublishingOffice publishingOffice;

    private String title;

    private String author;

    private Date lastSeen;

    private String hyperlink;

    public Long getId() {
        return id;
    }

    public PublishingOffice getPublishingOffice() {
        return publishingOffice;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public Date getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(Date lastSeen) {
        this.lastSeen = lastSeen;
    }

    public String getHyperlink() {
        return hyperlink;
    }

    public void setHyperlink(String hyperlink) {
        this.hyperlink = hyperlink;
    }

    protected AbstractBook(PublishingOffice publishingOffice, String title, String author, String hyperlink) {
        this.publishingOffice = publishingOffice;
        this.title = title;
        this.author = author;
        this.hyperlink = hyperlink;
    }
}
