package mephi.dozen.ais.entity.book;

import mephi.dozen.ais.entity.PublishingOffice;
import mephi.dozen.ais.entity.book.AbstractBook;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Date;

public class BookForLibrary extends AbstractBook {

    private Integer amountInLibrary;

    public BookForLibrary(PublishingOffice publishingOffice, String title, String author, Integer amountInLibrary, String hyperlink) {
        super(publishingOffice, title, author, hyperlink);
        this.amountInLibrary = amountInLibrary;
    }

    public Integer getAmountInLibrary() {
        return amountInLibrary;
    }

    public void changeAmountInLibrary(int number) {
        amountInLibrary += number;
    }

    public void setAmountInLibrary(Integer amountInLibrary) {
        this.amountInLibrary = amountInLibrary;
    }

}
