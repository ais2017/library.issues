package mephi.dozen.ais.entity.book;

import mephi.dozen.ais.entity.PublishingOffice;

public class BookForRequest extends AbstractBook {

    private Integer amount;

    private Boolean isEBook;

    public BookForRequest(PublishingOffice publishingOffice, String title, String author, Boolean isEBook, Integer amount, String hyperlink) {
        super(publishingOffice, title, author, hyperlink);
        this.amount = amount;
        this.isEBook = isEBook;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Boolean getEBook() {
        return isEBook;
    }

    public void setEBook(Boolean EBook) {
        isEBook = EBook;
    }
}
