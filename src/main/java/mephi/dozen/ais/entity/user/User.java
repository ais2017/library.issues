package mephi.dozen.ais.entity.user;


import org.apache.commons.codec.digest.DigestUtils;

public abstract class User {

    private Long id;

    private String login;

    private String name;

    private String password;

    private static Long counter = 0L;

    protected User(String login, String name, String password) {
        counter += 1;
        this.id = counter;
        this.login = login;
        this.name = name;
        this.password = DigestUtils.md5Hex(password);
    }

    public Long getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
