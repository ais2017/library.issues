package mephi.dozen.ais.info;

public class RegisterRequestInfo {

    public enum UserType {
        LIBRARIAN, BOOK_MANAGER, ADMINISTRATOR
    }

    private String login;

    private String name;

    private String password;

    private UserType userType;

    public RegisterRequestInfo(String login, String name, String password, UserType userType) {
        this.login = login;
        this.name = name;
        this.password = password;
        this.userType = userType;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }
}
