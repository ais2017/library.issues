package mephi.dozen.ais.response;

import java.util.ArrayList;

public class Response<T> {

    private T field;

    private ArrayList<String> errorMessages;

    public T getField() {
        return field;
    }

    public void setField(T field) {
        this.field = field;
    }

    public ArrayList<String> getErrorMessages() {
        return errorMessages;
    }

    public void addErrorMessage(String message) {
        if (errorMessages == null) {
            errorMessages = new ArrayList<>();
        }
        errorMessages.add(message);
    }

    public void setErrorMessages(ArrayList<String> errorMessages) {
        this.errorMessages = errorMessages;
    }
}
