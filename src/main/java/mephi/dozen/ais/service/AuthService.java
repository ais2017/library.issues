package mephi.dozen.ais.service;

import mephi.dozen.ais.AuthContext;
import mephi.dozen.ais.DataStorageConnector;
import mephi.dozen.ais.info.RegisterRequestInfo;
import mephi.dozen.ais.entity.user.Administrator;
import mephi.dozen.ais.entity.user.BookManager;
import mephi.dozen.ais.entity.user.Librarian;
import mephi.dozen.ais.entity.user.User;
import org.apache.commons.codec.digest.DigestUtils;

public class AuthService {

    public static User register(RegisterRequestInfo info) {

        switch (info.getUserType()) {
            case LIBRARIAN:
                return DataStorageConnector.saveUser(new Librarian(info.getLogin(), info.getName(), info.getPassword()));
            case BOOK_MANAGER:
                return DataStorageConnector.saveUser(new BookManager(info.getLogin(), info.getName(), info.getPassword()));
            case ADMINISTRATOR:
                return DataStorageConnector.saveUser(new Administrator(info.getLogin(), info.getName(), info.getPassword()));
        }
        return null;
    }

    public static User login (String login, String password) {
        User user = DataStorageConnector.getUser(login);
        if (user == null || !user.getPassword().equals(DigestUtils.md5Hex(password))) {
            return null;
        }
        AuthContext.login(user);
        return user;
    }

    public static User logout(String login) {
        return AuthContext.logout(login);
    }

    public static User update(RegisterRequestInfo info) {
        User user = DataStorageConnector.getUser(info.getLogin());
        if (info.getName() != null) {
            user.setName(info.getName());
        }
        if (info.getPassword() != null) {
            user.setPassword(DigestUtils.md5Hex(info.getPassword()));
        }
        return DataStorageConnector.updateUser(user);
    }

    public static User delete(String login) {
        return DataStorageConnector.deleteUser(login);
    }
}
