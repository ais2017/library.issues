package mephi.dozen.ais.service;

import mephi.dozen.ais.DataStorageConnector;
import mephi.dozen.ais.entity.ClientOrder;

public class ClientOrderService {

    public static ClientOrder create(ClientOrder clientOrder){
        clientOrder.setState(ClientOrder.CREATED);
        return DataStorageConnector.saveClientOrder(clientOrder);
    }

    public static ClientOrder delete(Long id) {
        return DataStorageConnector.deleteClientOrder(id);
    }
}
