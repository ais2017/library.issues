package mephi.dozen.ais.service;

import mephi.dozen.ais.DataStorageConnector;
import mephi.dozen.ais.entity.PublishingOffice;
import mephi.dozen.ais.entity.book.BookForLibrary;

import javax.xml.crypto.Data;

/**
 * @author Mitenkov Roman
 */
public class PublishingOfficeService {

    public static PublishingOffice add(PublishingOffice publishingOffice) {
        if (DataStorageConnector.getPublishingOffice(publishingOffice.getName()) == null) {
            return DataStorageConnector.savePublishingOffice(publishingOffice);
        }
        return null;
    }

    public static PublishingOffice update(PublishingOffice publishingOffice) {
        return DataStorageConnector.updatePublishingOffice(publishingOffice);
    }

    public static PublishingOffice delete(String name) {
        return DataStorageConnector.deletePublishingOffice(name);
    }

    public static PublishingOffice addBooks(PublishingOffice publishingOffice) {
        PublishingOffice entity = DataStorageConnector.getPublishingOffice(publishingOffice.getName());
        if (entity != null) {
            for (BookForLibrary book : publishingOffice.getBooks()) {
                if (!entity.checkIfBookAdded(book)){
                    entity.addBook(book);
                }
            }
            DataStorageConnector.updatePublishingOffice(entity);
            return entity;
        }
        return null;
    }

    public static PublishingOffice removeBooks(PublishingOffice publishingOffice) {
        PublishingOffice entity = DataStorageConnector.getPublishingOffice(publishingOffice.getName());
        if (entity != null) {
            for (BookForLibrary book : publishingOffice.getBooks()) {
                if (entity.checkIfBookAdded(book)){
                    entity.removeBook(book);
                }
            }
            DataStorageConnector.updatePublishingOffice(entity);
            return entity;
        }
        return null;
    }
}
