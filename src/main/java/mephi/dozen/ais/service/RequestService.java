package mephi.dozen.ais.service;

import mephi.dozen.ais.DataStorageConnector;
import mephi.dozen.ais.entity.Request;
import mephi.dozen.ais.entity.book.BookForRequest;

/**
 * @author Mitenkov Roman
 */
public class RequestService {

    public static Request send(Request request) {
        request.setState(Request.REQUEST_CREATED);
        if (sendRequest(request)) {
            request.setState(Request.REQUEST_SENDED);
            DataStorageConnector.saveRequest(request);
            return request;
        }
        DataStorageConnector.saveRequest(request);
        return null;
    }

    public static Request handleResponse(Request request) {
        request.setState(Request.REQUEST_FINISHED);
        DataStorageConnector.updateRequest(request);

        for (BookForRequest book : request.getBooks()) {
            DataStorageConnector.handleBookForRequest(book);
        }
        return request;
    }

    private static boolean sendRequest(Request request) {
        return true;
    }
}
