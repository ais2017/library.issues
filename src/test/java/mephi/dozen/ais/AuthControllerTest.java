package mephi.dozen.ais;

import mephi.dozen.ais.controller.AuthController;
import mephi.dozen.ais.entity.user.User;
import mephi.dozen.ais.info.RegisterRequestInfo;
import mephi.dozen.ais.response.Response;
import mephi.dozen.ais.service.AuthService;
import org.junit.Before;
import org.junit.Test;

import org.junit.runners.MethodSorters;
import static org.junit.Assert.*;

import org.junit.FixMethodOrder;

@FixMethodOrder(MethodSorters.JVM)
public class AuthControllerTest {

    @Before
    public void createAdminUser() {
        RegisterRequestInfo regInfo = new RegisterRequestInfo("admin", "Administrator",
                "password", RegisterRequestInfo.UserType.ADMINISTRATOR);
        AuthService.register(regInfo);
    }

    @Test
    public void loginLogoutTest() {
        Response<User> response = AuthController.login("admin", "wrong password");
        assertFalse(response.getErrorMessages().isEmpty());
        assertNull(AuthContext.getCurrentUser());

        response = AuthController.login("admin1", "password");
        assertFalse(response.getErrorMessages().isEmpty());
        assertNull(AuthContext.getCurrentUser());

        response = AuthController.login("admin", "password");
        assertNull(response.getErrorMessages());
        assertEquals(AuthContext.getCurrentUser().getLogin(), "admin");

        response = AuthController.logout("admin1");
        assertFalse(response.getErrorMessages().isEmpty());

        AuthController.logout("admin");
        assertNull(AuthContext.getCurrentUser());
    }

    @Test
    public void createUserTest() {
        //администратор входит в систему
        AuthController.login("admin", "password");

        //администратор создает нового пользователя
        RegisterRequestInfo regInfo1 = new RegisterRequestInfo("user1", "User 1",
                "password", RegisterRequestInfo.UserType.BOOK_MANAGER);
        Response<User> user1 = AuthController.register(regInfo1);
        assertNull(user1.getErrorMessages());
        assertNotNull(DataStorageConnector.getUser(user1.getField().getLogin()));

        //попытка создать второго пользователя с таким же логином
        user1 = AuthController.register(regInfo1);
        assertFalse(user1.getErrorMessages().isEmpty());

        //администратор вводит неверные данные и получает сообщения об ошибках
        RegisterRequestInfo regInfo2 = new RegisterRequestInfo(null, null, null, null);
        Response<User> user2 = AuthController.register(regInfo2);
        assertEquals(user2.getErrorMessages().size(), 4);
        assertNull(DataStorageConnector.getUser(regInfo2.getLogin()));

        RegisterRequestInfo regInfo3 = new RegisterRequestInfo("login login", "U",
                "1", RegisterRequestInfo.UserType.BOOK_MANAGER);
        Response<User> user3 = AuthController.register(regInfo3);
        assertEquals(user3.getErrorMessages().size(), 3);
        assertNull(DataStorageConnector.getUser(regInfo3.getLogin()));

        //администратор выходит из системы
        AuthController.logout("admin");

        //пользователь 1 входит в систему
        AuthController.login("user1", "password");

        //пользователь 1 пытается создать нового пользователя, но ему не хватает прав доступа
        RegisterRequestInfo regInfo4 = new RegisterRequestInfo("user4", "User 4",
        "password", RegisterRequestInfo.UserType.BOOK_MANAGER);
        Response<User> user4 = AuthController.register(regInfo4);
        assertTrue(user4.getErrorMessages().get(0).equals("Не достаточно прав для выполнения операции"));
        assertNull(DataStorageConnector.getUser(regInfo4.getLogin()));

        AuthController.logout("user1");
    }

    @Test
    public void updateUserTest() {
        //администратор создает пользователей user1 и user2
        AuthController.login("admin", "password");
        RegisterRequestInfo regInfo1 = new RegisterRequestInfo("user1", "User 1",
                "password", RegisterRequestInfo.UserType.BOOK_MANAGER);
        AuthController.register(regInfo1);
        RegisterRequestInfo regInfo2 = new RegisterRequestInfo("user2", "User 2",
                "password", RegisterRequestInfo.UserType.LIBRARIAN);
        AuthController.register(regInfo2);

        //администратор обновляет пользователя
        Response<User> userResponse;
        RegisterRequestInfo regInfoForUpdate = new RegisterRequestInfo(null, null,
                null, null);
        userResponse = AuthController.update(regInfoForUpdate);
        assertFalse(userResponse.getErrorMessages().isEmpty());
        assertNull(userResponse.getField());

        regInfoForUpdate.setLogin("user1");
        regInfoForUpdate.setName("User new name");
        userResponse = AuthController.update(regInfoForUpdate);
        assertNull(userResponse.getErrorMessages());
        assertEquals(userResponse.getField().getName(), regInfoForUpdate.getName());

        //администратор выходит из системы
        AuthController.logout("admin");

        //пользователь 1 входит в систему
        AuthController.login("user1", "password");

        //пользователь 1 обновляет пользователя 2
        regInfoForUpdate.setLogin("user2");
        userResponse = AuthController.update(regInfoForUpdate);
        assertTrue(userResponse.getErrorMessages().get(0).equals("Не достаточно прав для выполнения операции"));

        AuthController.logout("user1");
    }

    @Test
    public void deleteUserTest() {
        //администратор создает пользователей user1 и user2
        AuthController.login("admin", "password");
        RegisterRequestInfo regInfo1 = new RegisterRequestInfo("user1", "User 1",
                "password", RegisterRequestInfo.UserType.BOOK_MANAGER);
        AuthController.register(regInfo1);
        RegisterRequestInfo regInfo2 = new RegisterRequestInfo("user2", "User 2",
                "password", RegisterRequestInfo.UserType.LIBRARIAN);
        AuthController.register(regInfo2);

        //администратор выходит из системы
        AuthController.logout("admin");
        //пользователь 1 входит в систему
        AuthController.login("user1", "password");

        //пользователь 1 удаляет пользователя 2
        Response<User> userResponse;
        userResponse = AuthController.delete("user2");
        assertTrue(userResponse.getErrorMessages().get(0).equals("Не достаточно прав для выполнения операции"));

        //пользователь 1 выходит из системы
        AuthController.logout("user1");
        //администратор входит в систему
        AuthController.login("admin", "password");

        //администратор удаляет пользователя
        userResponse = AuthController.delete(null);
        assertFalse(userResponse.getErrorMessages().isEmpty());
        assertNull(userResponse.getField());

        userResponse = AuthController.delete("login");
        assertFalse(userResponse.getErrorMessages().isEmpty());
        assertNull(userResponse.getField());

        userResponse = AuthController.delete("user1");
        assertNull(userResponse.getErrorMessages());
        assertNull(DataStorageConnector.getUser("user1"));

        AuthController.logout("admin");
    }
}
