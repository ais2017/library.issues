package mephi.dozen.ais;

import ch.qos.logback.core.net.server.Client;
import mephi.dozen.ais.controller.AuthController;
import mephi.dozen.ais.controller.ClientOrderController;
import mephi.dozen.ais.controller.PublishingOfficeController;
import mephi.dozen.ais.controller.RequestController;
import mephi.dozen.ais.entity.ClientOrder;
import mephi.dozen.ais.entity.PublishingOffice;
import mephi.dozen.ais.entity.Request;
import mephi.dozen.ais.entity.book.BookForLibrary;
import mephi.dozen.ais.entity.book.BookForRequest;
import mephi.dozen.ais.info.RegisterRequestInfo;
import mephi.dozen.ais.response.Response;
import mephi.dozen.ais.service.AuthService;
import org.junit.Before;
import org.junit.Test;

import org.junit.runners.MethodSorters;
import static org.junit.Assert.*;

import org.junit.FixMethodOrder;

import javax.xml.crypto.Data;
import java.util.ArrayList;

@FixMethodOrder(MethodSorters.JVM)
public class ClientOrderControllerTest {

    PublishingOffice publishingOffice;

    @Before
    public void prepare() {
        RegisterRequestInfo regInfo1 = new RegisterRequestInfo("user1", "Библиотекарь",
                "password", RegisterRequestInfo.UserType.LIBRARIAN);
        AuthService.register(regInfo1);
        RegisterRequestInfo regInfo2 = new RegisterRequestInfo("user2", "Сотрудник фонда выдачи",
                "password", RegisterRequestInfo.UserType.BOOK_MANAGER);
        AuthService.register(regInfo2);

        AuthController.login("user2", "password");

        publishingOffice = new PublishingOffice("office1", "1234567890");
        PublishingOfficeController.addPublishingOffice(publishingOffice);
        BookForLibrary book1 = new BookForLibrary(publishingOffice, "title1", "author1", 5, null);
        BookForLibrary book2 = new BookForLibrary(publishingOffice, "title2", "author2", 1, "book2.ru");
        publishingOffice.addBook(book1);
        publishingOffice.addBook(book2);
        PublishingOfficeController.addBooksToPublishingOffice(publishingOffice);

        AuthController.logout("user2");
    }

    @Test
    public void createClientOrderAndRequest() {
        AuthController.login("user1", "password");

        BookForLibrary bookForLibrary = publishingOffice.getBooks().get(0);
        BookForRequest book1 = new BookForRequest(publishingOffice, bookForLibrary.getTitle(), bookForLibrary.getAuthor(),
                false, 5, null);
        bookForLibrary = publishingOffice.getBooks().get(1);
        BookForRequest book2 = new BookForRequest(publishingOffice, bookForLibrary.getTitle(), bookForLibrary.getAuthor(),
                false, 1, null);
        ArrayList<BookForRequest> booksForRequest = new ArrayList<>();
        booksForRequest.add(book1);
        booksForRequest.add(book2);

        ClientOrder clientOrder = new ClientOrder(ClientOrder.addBooks, booksForRequest);
        Response<ClientOrder> response = ClientOrderController.create(clientOrder);
        assertNull(response.getErrorMessages());
        assertNotNull(DataStorageConnector.getClientOrder(response.getField().getId()));

        AuthController.logout("user1");

        AuthController.login("user2", "password");

        Request request = new Request(publishingOffice);
        ClientOrder entity = DataStorageConnector.getClientOrder(response.getField().getId());
        request.setBooks(entity.getBooks());
        RequestController.sendRequest(request);

        request.getBooks().get(1).setHyperlink(publishingOffice.getBooks().get(1).getHyperlink());
        RequestController.handleResponse(request);
        assertEquals( 2, DataStorageConnector.getBooks().size());

        AuthController.logout("user2");
    }
}
