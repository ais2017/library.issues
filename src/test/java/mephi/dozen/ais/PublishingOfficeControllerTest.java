package mephi.dozen.ais;

import mephi.dozen.ais.controller.AuthController;
import mephi.dozen.ais.controller.PublishingOfficeController;
import mephi.dozen.ais.entity.PublishingOffice;
import mephi.dozen.ais.entity.book.BookForLibrary;
import mephi.dozen.ais.info.RegisterRequestInfo;
import mephi.dozen.ais.response.Response;
import mephi.dozen.ais.service.AuthService;
import org.junit.Test;

import org.junit.Before;
import org.junit.Test;

import org.junit.runners.MethodSorters;
import static org.junit.Assert.*;

import org.junit.FixMethodOrder;

@FixMethodOrder(MethodSorters.JVM)
public class PublishingOfficeControllerTest {

    @Before
    public void createAdminUser() {
        RegisterRequestInfo regInfo = new RegisterRequestInfo("admin", "Administrator",
                "password", RegisterRequestInfo.UserType.ADMINISTRATOR);
        AuthService.register(regInfo);
    }

    @Test
    public void addPublishingOffice() {
        AuthController.login("admin", "password");

        Response<PublishingOffice> response;
        PublishingOffice publishingOffice = new PublishingOffice("office1", "1234567890");
        response = PublishingOfficeController.addPublishingOffice(publishingOffice);
        assertNull(response.getErrorMessages());
        assertNotNull(DataStorageConnector.getPublishingOffice(publishingOffice.getName()));

        AuthController.logout("admin");
    }

    @Test
    public void updatePublishingOffice() {
        AuthController.login("admin", "password");

        PublishingOffice publishingOffice = new PublishingOffice("office1", "1234567890");
        PublishingOfficeController.addPublishingOffice(publishingOffice);

        Response<PublishingOffice> response;
        publishingOffice.setPhone("0987654321");
        response = PublishingOfficeController.update(publishingOffice);
        assertNull(response.getErrorMessages());
        assertEquals(DataStorageConnector.getPublishingOffice(publishingOffice.getName()).getPhone(), publishingOffice.getPhone());

        AuthController.logout("admin");
    }

    @Test
    public void deletePublishingOffice() {
        AuthController.login("admin", "password");

        PublishingOffice publishingOffice = new PublishingOffice("office1", "1234567890");
        PublishingOfficeController.addPublishingOffice(publishingOffice);

        Response<PublishingOffice> response;
        response = PublishingOfficeController.deletePublishingOffice("office1");
        assertNull(response.getErrorMessages());
        assertNull(DataStorageConnector.getPublishingOffice(publishingOffice.getName()));

        AuthController.logout("admin");
    }

    @Test
    public void addBooks() {
        AuthController.login("admin", "password");

        PublishingOffice publishingOffice = new PublishingOffice("office1", "1234567890");
        PublishingOfficeController.addPublishingOffice(publishingOffice);

        BookForLibrary book = new BookForLibrary(publishingOffice, "title", "author", 3, null);
        publishingOffice.addBook(book);

        Response<PublishingOffice> response;
        response = PublishingOfficeController.addBooksToPublishingOffice(publishingOffice);
        assertNull(response.getErrorMessages());
        assertTrue(DataStorageConnector.getPublishingOffice(publishingOffice.getName()).checkIfBookAdded(book));

        AuthController.logout("admin");
    }

    @Test
    public void removeBooks() {
        AuthController.login("admin", "password");

        PublishingOffice publishingOffice = new PublishingOffice("office1", "1234567890");
        PublishingOfficeController.addPublishingOffice(publishingOffice);

        BookForLibrary book = new BookForLibrary(publishingOffice, "title", "author", 3, null);
        publishingOffice.addBook(book);
        Response<PublishingOffice> response;
        PublishingOfficeController.addBooksToPublishingOffice(publishingOffice);
        publishingOffice.removeBook(book);
        response = PublishingOfficeController.removeBooksToPublishingOffice(publishingOffice);
        assertNull(response.getErrorMessages());
        assertFalse(DataStorageConnector.getPublishingOffice(publishingOffice.getName()).checkIfBookAdded(book));

        AuthController.logout("admin");
    }

}
