package mephi.dozen.ais;


import mephi.dozen.ais.controller.AuthController;
import mephi.dozen.ais.controller.PublishingOfficeController;
import mephi.dozen.ais.controller.RequestController;
import mephi.dozen.ais.entity.PublishingOffice;
import mephi.dozen.ais.entity.Request;
import mephi.dozen.ais.entity.book.BookForLibrary;
import mephi.dozen.ais.entity.book.BookForRequest;
import mephi.dozen.ais.info.RegisterRequestInfo;
import mephi.dozen.ais.response.Response;
import mephi.dozen.ais.service.AuthService;
import org.junit.Before;
import org.junit.Test;

import org.junit.runners.MethodSorters;
import static org.junit.Assert.*;

import org.junit.FixMethodOrder;

@FixMethodOrder(MethodSorters.JVM)
public class RequestControllerTest {

    PublishingOffice publishingOffice;

    @Before
    public void prepare() {
        RegisterRequestInfo regInfo = new RegisterRequestInfo("user", "Пользователь",
                "password", RegisterRequestInfo.UserType.BOOK_MANAGER);
        AuthService.register(regInfo);

        AuthController.login("user", "password");

        publishingOffice = new PublishingOffice("office1", "1234567890");
        PublishingOfficeController.addPublishingOffice(publishingOffice);
        BookForLibrary book1 = new BookForLibrary(publishingOffice, "title1", "author1", 5, null);
        BookForLibrary book2 = new BookForLibrary(publishingOffice, "title2", "author2", 1, "book2.ru");
        publishingOffice.addBook(book1);
        publishingOffice.addBook(book2);
        PublishingOfficeController.addBooksToPublishingOffice(publishingOffice);

        AuthController.logout("user");
    }

    @Test
    public void sendRequestTest() {
        AuthController.login("user", "password");

        Request request = new Request(publishingOffice);
        BookForLibrary bookForLibrary = publishingOffice.getBooks().get(0);
        BookForRequest book1 = new BookForRequest(publishingOffice, bookForLibrary.getTitle(), bookForLibrary.getAuthor(),
                false, 5, null);
        request.addBook(book1);
        bookForLibrary = publishingOffice.getBooks().get(1);
        BookForRequest book2 = new BookForRequest(publishingOffice, bookForLibrary.getTitle(), bookForLibrary.getAuthor(),
                false, 1, null);
        request.addBook(book2);
        Response<Request> response = RequestController.sendRequest(request);
        assertNull(response.getErrorMessages());

        request.getBooks().get(1).setHyperlink(publishingOffice.getBooks().get(1).getHyperlink());
        response = RequestController.handleResponse(request);
        assertNull(response.getErrorMessages());
        assertEquals( 2, DataStorageConnector.getBooks().size());

        AuthController.logout("user");
    }
}
