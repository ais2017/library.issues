package mephi.dozen.ais.office;

import mephi.dozen.ais.entity.book.BookForLibrary;
import mephi.dozen.ais.entity.PublishingOffice;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

public class PublishingOfficeTest {

   /* @Test
    public void constructorTest() {
        String name = "Office1";
        String phone = "+7(988)7776655";

        PublishingOffice publishingOffice = new PublishingOffice(name, phone);

        assertEquals(name, publishingOffice.getName());
        assertEquals(phone, publishingOffice.getPhone());
    }

    @Test
    public void addBooksTest() {
        PublishingOffice publishingOffice = new PublishingOffice("Office1", "+7(988)7776655");
        BookForLibrary book = new BookForLibrary(publishingOffice, "book1", "author1", new Date(), 100, "http://");

        assertEquals(0, publishingOffice.getBooks().size());
        assertEquals(false, publishingOffice.checkIfBookAdded(book));

        publishingOffice.addBook(book);

        assertEquals(1, publishingOffice.getBooks().size());
        assertEquals(true, publishingOffice.checkIfBookAdded(book));

        publishingOffice.addBook(book);

        assertEquals(1, publishingOffice.getBooks().size());
        assertEquals(true, publishingOffice.checkIfBookAdded(book));
    }

    @Test
    public void deleteBooksTest() {
        PublishingOffice publishingOffice = new PublishingOffice("Office1", "123");
        BookForLibrary bookForLibrary = new BookForLibrary(publishingOffice, "book1", "author1", new Date(), 100, "http://");

        publishingOffice.addBook(bookForLibrary);

        assertEquals(1, publishingOffice.getBooks().size());
        assertEquals(true, publishingOffice.checkIfBookAdded(bookForLibrary));

        publishingOffice.removeBook(bookForLibrary);

        assertEquals(0, publishingOffice.getBooks().size());
        assertEquals(false, publishingOffice.checkIfBookAdded(bookForLibrary));

        //проверка корректного завершения операции удаления книги, которой нет в массиве
        publishingOffice.removeBook(bookForLibrary);

        assertEquals(0, publishingOffice.getBooks().size());

    }
    */
}
