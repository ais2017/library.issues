package mephi.dozen.ais.request;

import mephi.dozen.ais.entity.book.BookForRequest;
import mephi.dozen.ais.entity.ClientOrder;
import mephi.dozen.ais.entity.PublishingOffice;
import mephi.dozen.ais.entity.user.BookManager;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;

public class ClientOrderTest {

    /*@Test
    public void constructorTest() {
        BookManager bookManager = new BookManager("user");
        String state = "Initial";
        String action = "Add";

        ClientOrder clientOrder = new ClientOrder(bookManager, state, action);

        assertEquals(state, clientOrder.getState());
        assertEquals(action, clientOrder.getAction());
    }

    @Test
    public void addBooksTest() {
        BookManager bookManager = new BookManager("user");
        PublishingOffice publishingOffice = new PublishingOffice("Office1", "123");
        ClientOrder clientOrder = new ClientOrder(bookManager,"Initial", "Add");
        BookForRequest book = new BookForRequest(publishingOffice, "book1", "author1", new Date(), false, 10);

        assertEquals(0, clientOrder.getBooks().size());
        assertEquals(false, clientOrder.checkIfBookAdded(book));

        clientOrder.addBook(book);

        assertEquals(1, clientOrder.getBooks().size());
        assertEquals(true, clientOrder.checkIfBookAdded(book));

        clientOrder.addBook(book);

        assertEquals(1, clientOrder.getBooks().size());
        assertEquals(true, clientOrder.checkIfBookAdded(book));
    }

    @Test
    public void deleteBooksTest() {
        BookManager bookManager = new BookManager("user");
        PublishingOffice publishingOffice = new PublishingOffice("Office1", "123");
        ClientOrder clientOrder = new ClientOrder(bookManager,"Initial", "Add");
        BookForRequest book = new BookForRequest(publishingOffice, "book1", "author1", new Date(), false, 10);

        clientOrder.addBook(book);

        assertEquals(1, clientOrder.getBooks().size());
        assertEquals(true, clientOrder.checkIfBookAdded(book));

        clientOrder.removeBook(book);

        assertEquals(0, clientOrder.getBooks().size());
        assertEquals(false, clientOrder.checkIfBookAdded(book));

        clientOrder.removeBook(book);

        assertEquals(0, clientOrder.getBooks().size());
    }*/
}
