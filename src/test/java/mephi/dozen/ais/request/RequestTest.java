package mephi.dozen.ais.request;

import mephi.dozen.ais.entity.book.BookForRequest;
import mephi.dozen.ais.entity.PublishingOffice;
import mephi.dozen.ais.entity.Request;
import mephi.dozen.ais.entity.user.BookManager;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

public class RequestTest {

    /*@Test
    public void constructorTest() {
        BookManager bookManager = new BookManager("user");
        PublishingOffice publishingOffice = new PublishingOffice("Office1", "123");
        String state = "Initial";

        Request request = new Request(bookManager, publishingOffice, state);

        assertEquals(bookManager, request.getCreatedBy());
        assertEquals(publishingOffice, request.getPublishingOffice());
        assertEquals(state, request.getState());
    }

    @Test
    public void addBooksTest() {
        BookManager bookManager = new BookManager("user");
        PublishingOffice publishingOffice = new PublishingOffice("Office1", "123");
        Request request = new Request(bookManager,publishingOffice, "Initial");
        BookForRequest book = new BookForRequest(publishingOffice, "book1", "author1", new Date(), false, 10);

        assertEquals(0, request.getBooks().size());
        assertEquals(false, request.checkIfBookAdded(book));

        request.addBook(book);

        assertEquals(1, request.getBooks().size());
        assertEquals(true, request.checkIfBookAdded(book));

        request.addBook(book);

        assertEquals(1, request.getBooks().size());
        assertEquals(true, request.checkIfBookAdded(book));
    }

    @Test
    public void deleteBooksTest() {
        BookManager bookManager = new BookManager("user");
        PublishingOffice publishingOffice = new PublishingOffice("Office1", "123");
        Request request = new Request(bookManager,publishingOffice, "Initial");
        BookForRequest book = new BookForRequest(publishingOffice, "book1", "author1", new Date(), false, 10);

        request.addBook(book);

        assertEquals(1, request.getBooks().size());
        assertEquals(true, request.checkIfBookAdded(book));

        request.removeBook(book);

        assertEquals(0, request.getBooks().size());
        assertEquals(false, request.checkIfBookAdded(book));

        request.removeBook(book);

        assertEquals(0, request.getBooks().size());
    }*/
}
